//http://uva.onlinejudge.org/external/103/10343.html
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void Base64(const char[],char [],int *);
void index_to_binary(int [],int, int *);
void convert_to_ASCII(int [],int ,int *);
void decode(int [],int);

const char table[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int main(){
    int i,len;
    char s[1000];
    while(gets(s)!=NULL){
        if(strcmp(s,"#")==0) printf("#");
        else if(strcmp(s,"##")==0) break;
        else{
            len=strlen(s);
            int index[len];
            int *binary=malloc(6*len*sizeof(int));
            for(i=0;i<6*len;i++){
                binary[i]=0;
            }
            Base64(table,s,index);//get the base64 index and store in index[]
            index_to_binary(index,len,binary);
            int output_digit=len*6/8;////一個字元6位元，合併後再拆成8位元一個
            int decimal_ascii[output_digit];
            for(i=0;i<output_digit;i++){
                decimal_ascii[i]=0;
            }
            convert_to_ASCII(binary,output_digit,decimal_ascii);
            decode(decimal_ascii,output_digit);
        }
    }
    return 0;
}

//get the base64 index
void Base64(const char table[],char s[], int *array){
    int i,j;
    for(i=0;i<strlen(s);i++){
        for(j=0;j<strlen(table);j++){
            if(s[i]=='0') array[i]=0;
            if(s[i]==table[j]) array[i]=j;
        }
    }
}

void index_to_binary(int index[],int len,int *b){
    int i;
    int n;
    for(i=0;i<len;i++){ //i:第幾個數字在做轉二進制
        int a[6]={0};//store the reverse binary sequence
        n=index[i];
        int j=0;
        while(n>0){
            a[j]=n%2;
            j+=1;
            n=n/2;
        }
        for(j=5;j>=0;j--){
            b[6*i+5-j]=a[j];
        }
    }
}

void convert_to_ASCII(int binary[],int output_digit, int *decimal){
    int i,j;
    for(i=0;i<output_digit;i++){
        for(j=0;j<8;j++){
            decimal[i]+=binary[8*i+7-j]*pow(2,j);
        }
    }
}

void decode(int decimal_ascii[],int output_digit){
    int i;
    for(i=0;i<output_digit;i++){
        printf("%c",decimal_ascii[i]);
    }
    printf("\n");
}