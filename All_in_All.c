//http://uva.onlinejudge.org/external/103/10340.html
#include <stdio.h>
#include <string.h>

char s[1000];
char t[1000];
int i,j;

int main(){
    while(scanf("%s %s",&s,&t)!=EOF){
        i=0;
        j=0;
        for(j=0;j<strlen(t)&&i<strlen(s);j++){
            if(s[i]==t[j]) i++;
        }
        if(i==strlen(s)) printf("Yes\n");
        else printf("No\n");
    }
    return 0;
}
//thought:
//<1>:s[i]==t[j] -> i++ & j++
//    s: s e q u e n c e
//index: i
//    t: s u b s e q u e n c e
//index: j
//<2>:s[i]!=t[j] -> j++
//    s: s e q u e n c e
//index:   i
//    t: s u b s e q u e n c e
//index:   j->
