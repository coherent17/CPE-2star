//http://uva.onlinejudge.org/external/102/10295.html
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct dict{
	char name[20];
	int price;
};

int main(){
	int m,n,i,j;
	scanf("%d %d",&m,&n);
	struct dict jobs[m];
	char str[1000];
	
	//construct the dictionary
	for(i=0;i<m;i++){
		scanf("%s %d",jobs[i].name,&jobs[i].price);
	}	
	//scanf the paragraph
	for(i=0;i<n;i++){
		int value=0;
		while(scanf("%s",str)==1){
			if(strcmp(str,".")==0){
				printf("%d\n",value);
				break;
			}
			else{
				for(j=0;j<m;j++){
					if(strcmp(str,jobs[j].name)==0) value+=jobs[j].price;
				}
			}
		}
	}
	return 0;
}

