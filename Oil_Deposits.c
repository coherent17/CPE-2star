//http://uva.onlinejudge.org/external/5/572.html
#include <stdio.h>
#include <stdbool.h>

int m,n;
char map[101][101]; //store the oil or not in the map
bool collect[101][101]; //store whether it has been collected or not
void collect_oil(int i,int j){
    //boundary condition
    if(i<0||j<0||i>=m||j>=n) return;
    //non oil region
    else if(map[i][j]=='*') return ;
    //check whether the oil region had been collected
    else if(collect[i][j]==true) return;
    else{
        collect[i][j]=true; //mark the oil region has been collected
        //start DFS (depth-first search)
        collect_oil(i-1,j-1); //left up
        collect_oil(i-1,j);   //left
        collect_oil(i-1,j+1); //left down
        collect_oil(i,j-1);   //up
        collect_oil(i,j+1);   //down
        collect_oil(i+1,j-1); //right up
        collect_oil(i+1,j);   //right
        collect_oil(i+1,j+1); //right down
    }
}
int main(){
    int i,j;
    while(scanf("%d %d",&m,&n)!=EOF){
        if(m==0 && n==0) break;
        //read the "\n"
        getchar();
        //initialize the collect to all false -> all of the region don't be collected
        for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                collect[i][j]=false;
            }
        }
        //read the map
        for(i=0;i<m;i++){
            gets(map[i]);
        }
        //out: count how many oil region
        int out=0;
        for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                if(map[i][j]=='@'&&collect[i][j]==false){
                    out+=1;
                    collect_oil(i,j);
                }
            }
        }
        printf("%d\n",out);
    }
    return 0;
}
