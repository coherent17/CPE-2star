//http://uva.onlinejudge.org/external/100/10013.html
#include <stdio.h>
#include <stdlib.h>

int main(){
    int n,digit;
    int i,j;
    scanf("%d",&n);
    for(i=0;i<n;i++){
        scanf("%d",&digit);
        int *a=malloc(digit*sizeof(int)); //store the left side number
        int *b=malloc(digit*sizeof(int)); //store the right side number
        int *carry=malloc(digit+1*sizeof(int)); //store the carry of the (a[j]+b[j]+carry[j])/10
        int *output=malloc(digit*sizeof(int)); //store the answer of the (a[j]+b[j]+carry[j])%10
        //initialize the carry to all 0
        for(j=0;j<digit;j++){
            carry[j]=0;
        }
        for(j=digit-1;j>=0;j--){
            scanf("%d %d",&a[j],&b[j]);
        }
        for(j=0;j<digit;j++){
            output[j]=(a[j]+b[j]+carry[j])%10;
            carry[j+1]=(a[j]+b[j]+carry[j])/10;
        }
        if(i!=0) printf("\n");
        if(carry[digit]!=0) printf("%d",carry[digit]);
        for(j=digit-1;j>=0;j--){
            printf("%d",output[j]);
        }
        printf("\n");
    }
    return 0;
}
