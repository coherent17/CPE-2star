//http://uva.onlinejudge.org/external/101/10106.html
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    char s1[250];
    char s2[250];
    int i,j;
    while(1){
        scanf("%s",s1);
        scanf("%s",s2);
        int n1[250]={0};
        int n2[250]={0};
        int answer[500]={0};
        //convert the string into bitwise array and reverse it
        for(i=0;i<strlen(s1);i++){
            n1[i]=s1[strlen(s1)-i-1]-'0';
        }
        for(i=0;i<strlen(s2);i++){
            n2[i]=s2[strlen(s2)-i-1]-'0';
        }
        for(i=0;i<strlen(s1);i++){
            for(j=0;j<strlen(s2);j++){
                answer[i+j]+=n1[i]*n2[j];
                answer[i+j+1]+=answer[i+j]/10; //if carry
                answer[i+j]=answer[i+j]%10;
            }
        }
        //output the answer
        //predict the answer's digit is count by length of s1 + s2
        int answer_length=strlen(s1)+strlen(s2);
        //until the higher digit isn't 0 -> output
        while(answer[answer_length-1]==0){
            answer_length--;
        }
        for(i=answer_length-1;i>=0;i--){
            printf("%d",answer[i]);
        }
        printf("\n");
    }
    return 0;
}
