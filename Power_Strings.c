//http://uva.onlinejudge.org/external/102/10298.html
#include <stdio.h>
#include <string.h>

char s[1000000];
int main(){
    int i,j,k,flag;
    while(scanf("%s",s)!=EOF){
        if(strcmp(s,".")==0) break;
        //the period of the repeat sequence
        for(i=1;i<=strlen(s);i++){
            if(strlen(s)%i!=0) continue;
            flag=1;
            //the begin of the second time of the a sequence is j
            for(j=i;j<strlen(s)&&flag==1;j+=i){
                for(k=0;k<i&&flag==1;k++){
                    if(s[k]!=s[k+j])flag=0;
                }
            }
            if(flag==1){
                printf("%d\n",strlen(s)/i);
                break;
            }
        }
    }
    return 0;
}
